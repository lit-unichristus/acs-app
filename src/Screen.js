import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  
  Platform,
  Image,
} from 'react-native';
import colors from './colors';


export default class Screen extends Component {
  static navigationOptions = {
    headerTitle: 'ACS App',
    headerStyle: {
      backgroundColor: colors.primaryColor,
    },
    headerTintColor: colors.lightColor,
  };

  

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          

          <View style={styles.cards}> 
          
          <TouchableHighlight
            underlayColor="white"
            onPress={() => {
              
              this.props.navigation.navigate('Form');
              
            }}>
            <Text style={styles.text}>CADASTRAR ACS</Text>
          </TouchableHighlight>
           </View>

           <View style={styles.cards}> 
          
          <TouchableHighlight
            underlayColor="white"
            onPress={() => {
              
              this.props.navigation.navigate('Search');
              
            }}>
            <Text style={styles.text}>BUSCAR ACS</Text>
          </TouchableHighlight>
           </View>

           <View style={styles.cards}> 
          
          <TouchableHighlight
            underlayColor="white"
            onPress={() => {
              
              this.props.navigation.navigate('Form2');
              
            }}>
            <Text style={styles.text}>CADASTRAR PACIENTE</Text>
          </TouchableHighlight>
           </View>

           <View style={styles.cards}> 
          
          <TouchableHighlight
            underlayColor="white"
            onPress={() => {
              
              this.props.navigation.navigate('Search2');
              
            }}>
            <Text style={styles.text}>BUSCAR PACIENTE</Text>
          </TouchableHighlight>
           </View>
        </View>

        

        

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
    backgroundColor: colors.grayColor,
    
  },

  title: {
    fontWeight: 'bold',
    color: colors.lightColor,
    textAlign: 'center',
    fontSize: 20,
    borderWidth: 0,
    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },

  flatListItem: {
    fontWeight: 'bold',
    color: colors.lightColor,
    textAlign: 'center',
    backgroundColor: colors.accentColor,
    borderWidth: 2,

    borderColor: colors.lightColor,
    //alignSelf: 'stretch',

    borderRadius: 5,
    padding: 20,
  },

  text: {
    fontWeight: 'bold',
    fontSize: 14,
    flex: 0,
    marginBottom: 30,
    marginTop: 30,
    color: colors.primaryColor,
    ...Platform.select({
      ios: {
        textAlign: 'justify',
      },
      android: {
        textAlign: 'center',
      },
    }),
  },

  body: {
    backgroundColor: colors.grayColor,
    
    padding: 20,
    marginBottom: 30,
    marginTop: 10,

    flex: 0,
    borderRadius: 10,

    justifyContent: 'center',
    alignItems: 'center',
  },

  cards: {
    flex:0,
    textAlign: 'center',
    backgroundColor: colors.lightColor,
    borderBottomWidth: 2,
    borderLeftWidth: 2,
    borderRightWidth: 2,
    borderTopWidth: 2,
    alignSelf: 'stretch',

    borderBottomColor: '#999',
    borderLeftColor: '#BBB',
    borderRightColor: '#AAA',
    borderTopColor: '#CCC',

    
    borderRadius: 5,
    padding: 10,
    marginBottom: 20,
  },
});
