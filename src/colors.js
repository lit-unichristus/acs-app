const colors = {
    primaryColor : "#24b3ae",
    primaryDarkColor : "#1b8985",
    accentColor : "#ff8e33",
    lightColor : "#FDFDFD",
    grayColor : "#F0EFF5",
    grayColorText : "#dbddde",
    graySmallColor : "#aaa",
    grayMediumColor : "#4E4E4E",
    grayDarkColor : "#161925",
    blackOpacity : "rgba(0, 0, 0, 0.75)"
  };
  
  export default colors;