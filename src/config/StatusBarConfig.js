import { StatusBar } from 'react-native';

StatusBar.setBackgroundColor("#1b8985");
StatusBar.setBarStyle("light-content");
