/*
  @flow
*/
import { Platform } from "react-native";
import colors from "../../colors";
import type { StackNavigatorConfig } from "react-navigation";

export const stackOptions: StackNavigatorConfig = {
  initialRouteName: "Splash", //"SignIn",
  headerMode: Platform.OS === "ios" ? "float" : "screen",
  defaultNavigationOptions: {
    headerBackTitle: null,
    headerStyle: {
      backgroundColor: colors.primaryColor
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold"
    }
  }
};

export const tabBarOptions = {
  tabBarPosition: "bottom",
  animationEnabled: false,
  swipeEnabled: false,
  tabBarOptions: {
    showIcon: true,
    showLabel: true,
    upperCaseLabel: false,
    activeTintColor: colors.accentColor,
    inactiveTintColor: colors.graySmallColor,
    style: {
      backgroundColor: colors.lightColor
    },
    indicatorStyle: {
      backgroundColor: colors.accentColor
    },
    labelStyle: {
      fontSize: 12,
      marginTop: 0,
      marginBottom: 0
    }
  }
};
