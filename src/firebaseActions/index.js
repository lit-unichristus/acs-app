import Firebase from 'firebase';
let config = {
  authDomain: 'https://acsapp-c89a4.firebaseio.com/',
  databaseURL: 'https://acsapp-c89a4.firebaseio.com/',
  projectId: 'acs.com',
};
let app = Firebase.initializeApp(config);
export const db = app.database();

/*export function getData() {
	const dataRef = database().ref('');

	return dataRef
		.once("value", (dataSnapshot) => {
			return new Promise((resolve, reject) => {
				const data = [];
				dataSnapshot.forEach((snap) => {
					data.push(snap.val());
				});
				console.warn(data.length);
				resolve(data);
			});
		});
}*/
