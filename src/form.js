/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  Platform,
  TextInput,
  Picker,
  Image,
} from 'react-native';
import colors from './colors';
import {db} from './firebaseActions';

export default class Screen extends Component {
  static navigationOptions = {
    headerTitle: 'Cadastro',
    headerStyle: {
      backgroundColor: colors.primaryColor,
    },
    headerTintColor: colors.lightColor,
  };

  constructor(props) {
    super(props);
    this.state = {
      fieldName: '',
      fieldAge: '',
      fieldCPF: '',
      fieldSex: '',
      fieldSchool: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <ScrollView>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
              }}>
              Cadastrar ACS
            </Text>
            <TextInput
              style={{
                height: 40,
                borderBottomColor: colors.primaryColor,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              onChangeText={text => this.setState({fieldName: text})}
              placeholder="Nome"
              value={this.state.fieldName}
            />

            <TextInput
              style={{
                height: 40,
                borderBottomColor: 'gray',
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              onChangeText={text => this.setState({fieldAge: text})}
              placeholder="Idade"
              value={this.fieldAge}
            />

            <TextInput
              style={{
                height: 40,
                borderBottomColor: colors.primaryColor,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              onChangeText={text => this.setState({fieldCPF: text})}
              placeholder="CPF:"
              value={this.state.fieldCPF}
            />

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Escolaridade:</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldSchool}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldSchool: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Ensino Médio" value="Ensino Médio" />
                <Picker.Item label="Ensino Superior" value="Ensino Superior" />
                <Picker.Item label="Pós Graduação" value="Pós Graduação" />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Sexo:</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldSex}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldSex: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Masculino" value="Masculino" />
                <Picker.Item label="Feminino" value="Feminino" />
              </Picker>
            </View>
          </ScrollView>
        </View>

        <View
          style={{
            flex: 0,
            alignItems: 'center',
            backgroundColor: colors.primaryColor,
          }}>
          <Text style={{fontSize: 5}} />

          <TouchableHighlight
            underlayColor="white"
            onPress={() => {
              const CPF = this.state.fieldCPF;
              const info = {
                
                fieldName: this.state.fieldName,
                fieldAge: this.state.fieldAge,
                fieldCPF: this.state.fieldCPF,
                fieldSex: this.state.fieldSex,
                fieldSchool: this.state.fieldSchool,
              };

              db.ref('/acs').child(CPF).set(info);
              this.props.navigation.navigate('Screen');
            }}>
            <Text style={styles.flatListItem}> CONTINUAR </Text>
          </TouchableHighlight>

          <Text style={{fontSize: 5}} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightColor,
    
  },

  title: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'justify',
    fontSize: 20,
    borderWidth: 0,

    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },
  flatListItem: {
   
    color: 'white',
    textAlign: 'center',
    backgroundColor: colors.primaryColor,
    borderWidth: 1,
    borderColor: colors.lightColor,
    //alignSelf: 'stretch',
    borderRadius: 2,
    padding: 8,
    
  },
  text: {
   
    color: colors.primaryColor,
    textAlign: 'justify',
    fontSize: 16,
    borderWidth: 0,

    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },

  textVer: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 16,
    left: 10,
    alignSelf: 'flex-start',
  },

  textAux: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'left',
    fontSize: 16,
    left: 10,
    alignSelf: 'flex-start',
    
    
  },
  body: {
    //backgroundColor: colors.primaryColor,
    borderWidth: 0,
    borderColor: '#DDD',
    padding: 20,
    marginBottom: 30,
    marginTop: 10,
    flex: 1,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cards: {
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    alignSelf: 'stretch',
    borderColor: '#DDD',
    borderRadius: 5,
    padding: 10,
    marginBottom: 20,
  },
});
