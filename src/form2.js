/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  Platform,
  TextInput,
  Picker,
  Image,
} from 'react-native';
import colors from './colors';
import {db} from './firebaseActions';

export default class Screen extends Component {
  static navigationOptions = {
    headerTitle: 'Cadastro',
    headerStyle: {
      backgroundColor: colors.primaryColor,
    },
    headerTintColor: colors.lightColor,
  };

  constructor(props) {
    super(props);
    this.state = {
      fieldName: '',
      fieldAge: '',
      fieldCPF: '',
      fieldAdress: '',
      fieldNumber: '',
      fieldBairro: '',
      fieldSex: '',
      fieldRace: '',
      fieldSchool: '',
      fieldMarried: '',
      fieldWork: '',
      fieldSmoking: '',
      fieldSmokingN: '',
      fieldSmokingK: '',
      fieldAlcohol: '',
      fieldAlcoholT: '',
      fieldAlcoholF: '',
      fieldALcoholK: '',
      fieldHurts: '',
      risk: 1,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <ScrollView>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
              }}>
              Cadastrar Paciente
            </Text>
            <TextInput
              style={{
                height: 40,
                borderBottomColor: colors.primaryColor,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              onChangeText={text => this.setState({fieldName: text})}
              placeholder="Nome"
              value={this.state.fieldName}
            />

            <TextInput
              style={{
                height: 40,
                borderBottomColor: 'gray',
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              keyboardType="number-pad"
              onChangeText={text => this.setState({fieldAge: text})}
              placeholder="Idade"
              value={this.state.fieldAge}
            />

            <TextInput
              style={{
                height: 40,
                borderBottomColor: colors.primaryColor,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              keyboardType="number-pad"
              onChangeText={text => this.setState({fieldCPF: text})}
              placeholder="CPF:"
              value={this.state.fieldCPF}
            />

            <TextInput
              style={{
                height: 40,
                borderBottomColor: colors.primaryColor,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              onChangeText={text => this.setState({fieldAdress: text})}
              placeholder="Endereço:"
              value={this.state.fieldAdress}
            />

            <TextInput
              style={{
                height: 40,
                borderBottomColor: colors.primaryColor,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              keyboardType="number-pad"
              onChangeText={text => this.setState({fieldNumber: text})}
              placeholder="Número:"
              value={this.state.fieldNumber}
            />

            <TextInput
              style={{
                height: 40,
                borderBottomColor: colors.primaryColor,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              onChangeText={text => this.setState({fieldBairro: text})}
              placeholder="Bairro:"
              value={this.state.fieldBairro}
            />

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Sexo:</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldSex}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldSex: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Masculino" value="Masculino" />
                <Picker.Item label="Feminino" value="Feminino" />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Raça:</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldRace}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldRace: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Branco" value="Branco" />
                <Picker.Item label="Negro" value="Negro" />
                <Picker.Item label="Pardo" value="Pardo" />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Escolaridade:</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldSchool}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldSchool: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Analfabeto" value="Analfabeto" />
                <Picker.Item
                  label="Ensino Fundamental"
                  value="Ensino Fundamental"
                />
                <Picker.Item label="Ensino Médio" value="Ensino Médio" />
                <Picker.Item label="Ensino Superior" value="Ensino Superior" />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Estado Civil:</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldMarried}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldMarried: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Casado" value="Casado" />
                <Picker.Item
                  label="Relacionamento Estável"
                  value="Relacionamento Estável"
                />
                <Picker.Item label="Solteiro" value="Solteiro" />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Ocupação:</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldWork}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldWork: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Trabalho Formal" value="Trabalho Formal" />
                <Picker.Item
                  label="Trabalho Informal"
                  value="Trabalho Informal"
                />
                <Picker.Item label="Desempregado" value="Desempregado" />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Fumante:</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldSmoking}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldSmoking: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Nunca" value="Nunca" />
                <Picker.Item label="Ex-Fumante" value="Ex-Fumante" />
                <Picker.Item
                  label="fuma a menos de 20 anos"
                  value="fuma a menos de 20 anos"
                />
                <Picker.Item
                  label="fuma a mais de 20 anos"
                  value="fuma a mais de 20 anos"
                />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Quantidade de Cigarros:</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldSmokingN}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldSmokingN: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Não fuma" value="Não fuma" />
                <Picker.Item label="Até 20 por dia" value="Até 20 por dia" />
                <Picker.Item
                  label="Mais de 20 por dia"
                  value="Mais de 20 por dia"
                />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Tipo de cigarro</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldSmokingK}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldSmokingK: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Nenhum" value="Nenhum" />
                <Picker.Item label="Cigarro" value="Cigarro" />
                <Picker.Item
                  label="Cachimbo ou outros"
                  value="Cachimbo ou outros"
                />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Consumo de Alcool</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldAlcohol}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldAlcohol: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Não bebe" value="Não bebe" />
                <Picker.Item label="Ex-consumidor" value="Ex-consumidor" />
                <Picker.Item
                  label="Bebe a menos de 20 anos"
                  value="Bebe a menos de 20 anos"
                />
                <Picker.Item
                  label="Bebe a mais de 20 anos"
                  value="Bebe a mais de 20 anos"
                />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Frequencia de Alcool</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldAlcoholF}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldAlcoholF: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item
                  label="Bebe até 2 vezes por semana"
                  value="Bebe até 2 vezes por semana"
                />
                <Picker.Item
                  label="Bebe mais de 2 vezes por semana"
                  value="Bebe mais de 2 vezes por semana"
                />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Tipo de Alcool</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldALcoholK}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldALcoholK: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item
                  label="Bebidas Fermentadas"
                  value="Bebidas Fermentadas"
                />
                <Picker.Item
                  label="Bebidas destiladas"
                  value="Bebidas destiladas"
                />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Possui ferimentos na boca?</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldHurts}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldHurts: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Sim" value="Sim" />
                <Picker.Item label="Não" value="Não" />
              </Picker>
            </View>

            <View
              style={{
                marginLeft: 30,
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text>Possui familiares com câncer?</Text>
              <Picker
                mode="dropdown"
                selectedValue={this.state.fieldCancer}
                style={{height: 50, width: 150, alignSelf: 'center'}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({fieldCancer: itemValue})
                }>
                <Picker.Item label="Selecionar..." value="" />
                <Picker.Item label="Sim" value="Sim" />
                <Picker.Item label="Não" value="Não" />
              </Picker>
            </View>
          </ScrollView>
        </View>

        <View
          style={{
            flex: 0,
            alignItems: 'center',
            backgroundColor: colors.primaryColor,
          }}>
          <Text style={{fontSize: 5}} />

          <TouchableHighlight
            underlayColor="white"
            onPress={() => {
              const CPF = this.state.fieldCPF;

              if (
                this.state.fieldRace == 'Negro' ||
                this.state.fieldRace == 'Pardo'
              ) {
                this.state.risk += 0.33;
              }
              if (this.state.Married == 'Solteiro') {
                this.state.risk += 0.75;
              }
              if (
                this.state.School == 'Ensino Fundamental' ||
                this.state.School == 'Analfabeto'
              ) {
                this.state.risk += 1.75;
              }
              if (this.state.Work == 'Desempregado') {
                this.state.risk += 0.85;
              }
              if (this.state.Smoking == 'Ex-Fumante') {
                this.state.risk += 0.38;
              }
              if (this.state.Smoking == 'Fuma a menos de vinte anos') {
                this.state.risk += 3.45 + 0.64;
              }
              if (this.state.Smoking == 'Fuma a mais de vinte anos') {
                this.state.risk += 3.45 + 6.24;
              }
              if (this.state.SmokingN == 'Até 20 por dia') {
                this.state.risk += 0.74;
              }
              if (this.state.SmokingN == 'Mais de 20 por dia') {
                this.state.risk += 4.59;
              }
              if (this.state.SmokingK == 'Cigarro') {
                this.state.risk += 0.09;
              }
              if (this.state.SmokingK == 'Cachimbo ou outros') {
                this.state.risk += 3.06;
              }
              if (this.state.Alcohol == 'Ex-Consumidor') {
                this.state.risk += 0.07;
              }
              if (this.state.Alcohol == 'Bebe a menos de 20 anos') {
                this.state.risk += 1.73 + 1.85;
              }
              if (this.state.Alcohol == 'Bebe a mais de 20 anos') {
                this.state.risk += 1.73 + 2.53;
              }
              if (this.state.AlcoholF == 'Bebe até 2 vezes por semana') {
                this.state.risk += 0.73;
              }
              if (this.state.AlcoholF == 'Bebe mais de 2 vezes por semana') {
                this.state.risk += 4.54;
              }
              if (this.state.AlcoholK == 'Bebidas Fermentadas') {
                this.state.risk += 0.25;
              }
              if (this.state.AlcoholK == 'Bebidas destiladas') {
                this.state.risk += 4.58;
              }

              const info = {
                fieldName: this.state.fieldName,
                fieldAge: this.state.fieldAge,
                fieldCPF: this.state.fieldCPF,
                fieldAdress: this.state.fieldAdress,
                fieldNumber: this.state.fieldNumber,
                fieldBairro: this.state.fieldBairro,
                fieldSex: this.state.fieldSex,
                fieldRace: this.state.fieldRace,
                fieldSchool: this.state.fieldSchool,
                fieldMarried: this.state.fieldMarried,
                fieldWork: this.state.fieldWork,
                fieldSmoking: this.state.fieldSmoking,
                fieldSmokingN: this.state.fieldSmokingN,
                fieldSmokingK: this.state.fieldSmokingK,
                fieldAlcohol: this.state.fieldAlcohol,
                fieldAlcoholF: this.state.fieldAlcoholF,
                fieldALcoholK: this.state.fieldALcoholK,
                fieldHurts: this.state.fieldHurts,
                risk: this.state.risk,
              };

              db.ref('/patient')
                .child(CPF)
                .set(info);
              this.props.navigation.navigate('Screen');
            }}>
            <Text style={styles.flatListItem}> CONTINUAR </Text>
          </TouchableHighlight>

          <Text style={{fontSize: 5}} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightColor,
  },

  title: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'justify',
    fontSize: 20,
    borderWidth: 0,

    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },
  flatListItem: {
    color: 'white',
    textAlign: 'center',
    backgroundColor: colors.primaryColor,
    borderWidth: 1,
    borderColor: colors.lightColor,
    //alignSelf: 'stretch',
    borderRadius: 2,
    padding: 8,
  },
  text: {
    color: colors.primaryColor,
    textAlign: 'justify',
    fontSize: 16,
    borderWidth: 0,

    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },

  textVer: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'left',
    fontSize: 16,
    left: 10,
    alignSelf: 'flex-start',
  },

  textAux: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'left',
    fontSize: 16,
    left: 10,
    alignSelf: 'flex-start',
  },
  body: {
    //backgroundColor: colors.primaryColor,
    borderWidth: 0,
    borderColor: '#DDD',
    padding: 20,
    marginBottom: 30,
    marginTop: 10,
    flex: 1,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cards: {
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    alignSelf: 'stretch',
    borderColor: '#DDD',
    borderRadius: 5,
    padding: 10,
    marginBottom: 20,
  },
});
