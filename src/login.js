import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  Picker,
  Platform,
  Image,
} from 'react-native';
import colors from './colors';

export default class Screen extends Component {
  static navigationOptions = {
    headerTitle: 'ACS App',
    headerStyle: {
      backgroundColor: colors.primaryColor,
    },
    headerTintColor: colors.lightColor,
  };

  

  
         
  

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <Image
            style={{width: 100, height: 100}}
            source={require('./title.png')}
          />
          <Text style={styles.title}>Bem Vindo!</Text>

          <Text style={styles.textVer}>Versão</Text>
          <Text style={styles.textAux}>Alpha 0.1</Text>
          

          <Text style={styles.text}>Olá! O ACS App é um aplicativo que te ajudará a decidir que paciente apresenta fatores de risco do desenvolvimento de câncer de boca.
Nosso intuito é identificar os pacientes de risco com o objetivo de encaminhá-lo para o cirurgião-dentista da Unidade Básica de Saúde da sua Regional para realização de uma consulta preventiva.
</Text>
          

          

        </View>

      

        <View
	          style ={{
            
            
	          flex:0,
            alignItems:'center',
            backgroundColor: colors.primaryColor,
            
	          }}>
          
          
        <Text style={{fontSize: 5}}></Text>

          <TouchableHighlight
            underlayColor="white"
            onPress={() => {
              
              this.props.navigation.navigate('Screen');
              
            }}>
            <Text style={styles.flatListItem}>        CONTINUAR        </Text>
          </TouchableHighlight>

          
        <Text style={{fontSize: 5}}></Text>

        </View>
      
        

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightColor,
    
  },

  title: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'justify',
    fontSize: 20,
    borderWidth: 0,

    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },
  flatListItem: {
   
    color: 'white',
    textAlign: 'center',
    backgroundColor: colors.primaryColor,
    borderWidth: 1,
    borderColor: colors.lightColor,
    //alignSelf: 'stretch',
    borderRadius: 2,
    padding: 8,
    
  },
  text: {
   
    color: colors.primaryColor,
    textAlign: 'justify',
    fontSize: 16,
    borderWidth: 0,

    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },

  textVer: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 16,
    left: 10,
    alignSelf: 'flex-start',
  },

  textAux: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'left',
    fontSize: 16,
    left: 10,
    alignSelf: 'flex-start',
    
    
  },
  body: {
    //backgroundColor: colors.primaryColor,
    borderWidth: 0,
    borderColor: '#DDD',
    padding: 20,
    marginBottom: 30,
    marginTop: 10,
    flex: 1,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cards: {
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    alignSelf: 'stretch',
    borderColor: '#DDD',
    borderRadius: 5,
    padding: 10,
    marginBottom: 20,
  },
});
