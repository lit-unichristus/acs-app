import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import {Platform} from 'react-native';
// 11 significa 1.1, 12 significa 1.2 etc
import stackOptions from './config/navigation/NavigationOptions';
import tabBarOptions from './config/navigation/NavigationOptions';

import Splash from './splash';
import Screen from './Screen';
import Login from './login';
import Form from './form';
import Form2 from './form2';
import Search from './search';
import Search2 from './search2';

const mainTabs = createBottomTabNavigator(
  
  {
    
    BasicTab: {
      screen: Screen,
    },
    GraphTab: {
      screen: Screen,
    },
    
  },
  
  tabBarOptions,
);

mainTabs.navigationOptions = ({navigation}) => {
  const {routeName} = navigation.state.routes[navigation.state.index];
  let headerTitle;
  if (routeName === 'BasicTab') {
    headerTitle = 'Córneas Disponíveis';
  } else if (routeName === 'GraphTab') {
    headerTitle = 'Gráficos';
  }

  return {
    headerTitle,
  };
};

/*const stackOptions = {
    initialRouteName: 'Splash',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: colors.primaryColor,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
}*/

const MainPage = createStackNavigator(
  {
    Splash,
    Login,
    Screen,
    Form,
    Form2,
    Search,
    Search2,
  },
  stackOptions,
);

export default createAppContainer(MainPage);
