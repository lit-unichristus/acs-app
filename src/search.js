/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  Platform,
  TextInput,
  Picker,
  Image,
} from 'react-native';
import colors from './colors';
import {db} from './firebaseActions';

export default class Screen extends Component {
  static navigationOptions = {
    headerTitle: 'Buscar ACS',
    headerStyle: {
      backgroundColor: colors.primaryColor,
    },
    headerTintColor: colors.lightColor,
  };

  constructor(props) {
    super(props);
    this.state = {
      fieldName: 'NULL',
      fieldAge: '',
      fieldCPF: '',
      fieldSex: '',
      fieldSchool: '',
    };
  }

  render() {
    var i;
    i++;
    if (i > 100) {
      this.forceUpdate();
      i = 0;
    }

    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <ScrollView>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
              }}>
              Digite o CPF
            </Text>
            <TextInput
              style={{
                height: 40,
                borderBottomColor: colors.primaryColor,
                width: 160,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              onChangeText={text => this.setState({fieldCPF: text})}
              placeholder="CPF"
              value={this.state.fieldCPF}
            />

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldName}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldAge}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldSchool}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldSex}
            </Text>
          </ScrollView>
        </View>

        <View
          style={{
            flex: 0,
            alignItems: 'center',
            backgroundColor: colors.primaryColor,
          }}>
          <Text style={{fontSize: 5}} />

          <TouchableHighlight
            underlayColor="white"
            onPress={() => {
              var Name;
              var School;
              var Sex;
              var Age;

              db.ref('acs/' + this.state.fieldCPF).on('value', function(
                snapshot,
              ) {
                Name = snapshot.val().fieldName;
                School = snapshot.val().fieldSchool;
                Sex = snapshot.val().fieldSex;
                Age = snapshot.val().fieldAge;
              });
              this.state.fieldSchool = School;
              this.state.fieldSex = Sex;
              this.state.fieldAge = Age + ' anos';
              this.state.fieldName = Name;
              this.forceUpdate();
            }}>
            <Text style={styles.flatListItem}> BUSCAR </Text>
          </TouchableHighlight>

          <Text style={{fontSize: 5}} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightColor,
  },

  title: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'justify',
    fontSize: 20,
    borderWidth: 0,

    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },
  flatListItem: {
    color: 'white',
    textAlign: 'center',
    backgroundColor: colors.primaryColor,
    borderWidth: 1,
    borderColor: colors.lightColor,
    //alignSelf: 'stretch',
    borderRadius: 2,
    padding: 8,
  },
  text: {
    color: colors.primaryColor,
    textAlign: 'justify',
    fontSize: 16,
    borderWidth: 0,

    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },

  textVer: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 16,
    left: 10,
    alignSelf: 'flex-start',
  },

  textAux: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'left',
    fontSize: 16,
    left: 10,
    alignSelf: 'flex-start',
  },
  body: {
    //backgroundColor: colors.primaryColor,
    borderWidth: 0,
    borderColor: '#DDD',
    padding: 20,
    marginBottom: 30,
    marginTop: 10,
    flex: 1,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cards: {
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    alignSelf: 'stretch',
    borderColor: '#DDD',
    borderRadius: 5,
    padding: 10,
    marginBottom: 20,
  },
});
