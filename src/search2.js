/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  Platform,
  TextInput,
  Picker,
  Image,
} from 'react-native';
import colors from './colors';
import {db} from './firebaseActions';

export default class Screen extends Component {
  static navigationOptions = {
    headerTitle: 'Buscar Paciente',
    headerStyle: {
      backgroundColor: colors.primaryColor,
    },
    headerTintColor: colors.lightColor,
  };

  constructor(props) {
    super(props);
    this.state = {
      fieldName: '',
      fieldAge: '',
      fieldCPF: '',
      fieldAdress: '',
      fieldNumber: '',
      fieldBairro: '',
      fieldSex: '',
      fieldRace: '',
      fieldSchool: '',
      fieldMarried: '',
      fieldWork: '',
      fieldSmoking: '',
      fieldSmokingN: '',
      fieldSmokingK: '',
      fieldAlcohol: '',
      fieldAlcoholF: '',
      fieldALcoholK: '',
      fieldHurts: '',
      fieldRisk: '',
    };
  }

  render() {
    var i;
    i++;
    if (i > 100) {
      this.forceUpdate();
      i = 0;
    }

    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <ScrollView>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
              }}>
              Digite o CPF
            </Text>
            <TextInput
              style={{
                height: 40,
                borderBottomColor: colors.primaryColor,
                width: 160,
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                marginBottom: 10,
                borderBottomWidth: 1,
              }}
              onChangeText={text => this.setState({fieldCPF: text})}
              placeholder="CPF"
              value={this.state.fieldCPF}
            />

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldName}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldAge}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldSchool}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldSex}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldAdress}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldNumber}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldBairro}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldRace}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldMarried}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldWork}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldSmoking}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldSmokingN}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldSmokingK}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldAlcohol}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldAlcoholK}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldAlcoholF}
            </Text>

            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                textAlign: 'center',
                marginTop: 20,
                color: colors.primaryColor,
                flex: 0,
              }}>
              {this.state.fieldHurts}
            </Text>



          </ScrollView>
        </View>

        <View
          style={{
            flex: 0,
            alignItems: 'center',
            backgroundColor: colors.primaryColor,
          }}>
          <Text style={{fontSize: 5}} />

          <TouchableHighlight
            underlayColor="white"
            onPress={() => {
              var Name;
              var School;
              var Sex;
              var Age;
              var Adress;
              var Numbero;
              var Bairro;
              var Race;
              var Work;
              var Married;
              var Smoking;
              var SmokingK;
              var SmokingN;
              var Alcohol;
              var AlcoholK;
              var AlcoholF;
              var Risk;

              db.ref('patient/' + this.state.fieldCPF).on('value', function(
                snapshot,
              ) {
                Name = snapshot.val().fieldName;
                School = snapshot.val().fieldSchool;
                Sex = snapshot.val().fieldSex;
                Age = snapshot.val().fieldAge;
                Adress = snapshot.val().fieldAdress;
                Numbero = snapshot.val().fieldNumber;
                Bairro = snapshot.val().fieldBairro;
                Race = snapshot.val().fieldRace;
                Work = snapshot.val().fieldWork;
                Married = snapshot.val().fieldMarried;
                Smoking = snapshot.val().fieldSmoking;
                SmokingK = snapshot.val().fieldSmokingK;
                SmokingN = snapshot.val().fieldSmokingN;
                Alcohol = snapshot.val().fieldAlcohol;
                AlcoholK = snapshot.val().fieldAlcoholK;
                AlcoholF = snapshot.val().fieldAlcoholF;
                Risk = snapshot.val().fieldRisk;

              });
              this.state.fieldSchool = School;
              this.state.fieldSex = Sex;
              this.state.fieldAge = Age + ' anos';
              this.state.fieldName = Name;
              this.state.fieldAdress = Adress;
              this.state.fieldNumber = Numbero;
              this.state.fieldBairro = Bairro;
              this.state.fieldRace = Race;
              this.state.fieldMarried = Married;
              this.state.fieldWork = Work;
              this.state.fieldSmoking = 'Fuma ' + Smoking;
              this.state.fieldSmokingN = SmokingK;
              this.state.fieldSmokingK = SmokingN;
              this.state.fieldAlcohol = Alcohol;
              this.state.fieldAlcoholF = AlcoholF;
              this.state.fieldALcoholK = AlcoholK;
              this.state.fieldRisk = Risk;
              this.forceUpdate();
            }}>
            <Text style={styles.flatListItem}> BUSCAR </Text>
          </TouchableHighlight>

          <Text style={{fontSize: 5}} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightColor,
  },

  title: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'justify',
    fontSize: 20,
    borderWidth: 0,

    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },
  flatListItem: {
    color: 'white',
    textAlign: 'center',
    backgroundColor: colors.primaryColor,
    borderWidth: 1,
    borderColor: colors.lightColor,
    //alignSelf: 'stretch',
    borderRadius: 2,
    padding: 8,
  },
  text: {
    color: colors.primaryColor,
    textAlign: 'justify',
    fontSize: 16,
    borderWidth: 0,

    padding: 20,
    marginBottom: 10,
    alignSelf: 'center',
  },

  textVer: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 16,
    left: 10,
    alignSelf: 'flex-start',
  },

  textAux: {
    fontWeight: 'bold',
    color: colors.primaryColor,
    textAlign: 'left',
    fontSize: 16,
    left: 10,
    alignSelf: 'flex-start',
  },
  body: {
    //backgroundColor: colors.primaryColor,
    borderWidth: 0,
    borderColor: '#DDD',
    padding: 20,
    marginBottom: 30,
    marginTop: 10,
    flex: 1,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cards: {
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    alignSelf: 'stretch',
    borderColor: '#DDD',
    borderRadius: 5,
    padding: 10,
    marginBottom: 20,
  },
});
