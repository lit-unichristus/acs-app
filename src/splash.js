import React, {Component} from 'react';
import {
  View,
  StatusBar,
  TouchableOpacity,
  StyleSheet,
  Image,
  Text,
} from 'react-native';
import colors from './colors';

export default class Splash extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: colors.primaryColor,
      shadowColor: 'transparent',
      shadowRadius: 0,
      elevation: 0,
      shadowOffset: {
        height: 0,
      },
    },
    headerTintColor: colors.lightColor,
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={colors.primaryDarkColor}
          barStyle="light-content"
        />
        <View style={styles.body}>
          <TouchableOpacity
            style={styles.body}
            onPress={() => {
              this.props.navigation.navigate('Login');
            }}>
            <Image
              style={{resizeMode: 'center'}}
              source={require('./title.png')}
            />
          </TouchableOpacity>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 100,
              fontSize: 24,
              color: '#FFFFFF',
            }}>
            ACS app 0.1
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primaryColor,
    padding: 20,
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    borderWidth: 0,
    padding: 20,
    marginBottom: 20,
    alignSelf: 'center',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    color: colors.lightColor,
  },
  midbuttons: {
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    alignSelf: 'stretch',

    borderColor: '#DDD',
    borderRadius: 5,
    padding: 10,
    marginBottom: 20,
  },
  body: {
    borderWidth: 0,
    marginBottom: 20,
    alignSelf: 'center',
    flex: 0.85,
    borderRadius: 10,
    justifyContent: 'center',
  },
  cards: {
    //flex-end,
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    borderColor: '#A0A0A0',
    borderRadius: 50,
    padding: 20,
    marginBottom: 20,
    position: 'absolute',
    bottom: 1,
    alignSelf: 'center',
  },
  next: {
    //flex-end,
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    borderColor: '#A0A0A0',
    borderRadius: 50,
    padding: 20,
    marginBottom: 20,
    position: 'absolute',
    bottom: 1,
    right: 1,
  },
  previous: {
    //flex-end,
    textAlign: 'center',
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    borderColor: '#A0A0A0',
    borderRadius: 50,
    padding: 20,
    marginBottom: 20,
    position: 'absolute',
    bottom: 1,
    left: 1,
  },
});
